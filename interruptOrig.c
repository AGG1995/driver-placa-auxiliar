#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/miscdevice.h> // misc dev
#include <linux/fs.h>         // file operations
#include <asm/uaccess.h>      // copy to/from user space
#include <linux/wait.h>       // waiting queue
#include <linux/sched.h>      // TASK_INTERRUMPIBLE
#include <linux/delay.h>      // udelay

#include <linux/interrupt.h>
#include <linux/gpio.h>

#define DRIVER_AUTHOR "Andres Rodriguez - DAC"
#define DRIVER_DESC   "Ejemplo interrupción para placa lab. DAC Rpi"

//GPIOS numbers as in BCM RPi

#define GPIO_BUTTON1 2
#define GPIO_BUTTON2 3

#define GPIO_SPEAKER 4

#define GPIO_GREEN1  27
#define GPIO_GREEN2  22
#define GPIO_YELLOW1 17
#define GPIO_YELLOW2 11
#define GPIO_RED1    10
#define GPIO_RED2    9

static int LED_GPIOS[] = {GPIO_GREEN1, GPIO_GREEN2, GPIO_YELLOW1, GPIO_YELLOW2, GPIO_RED1, GPIO_RED2} ;

static int SPEAKER_GPIO = GPIO_SPEAKER;

//static int BUTTON_GPIOS[]= {GPIO_BUTTON1, GPIO_BUTTON2};

static char *led_desc[] = {"GPIO_GREEN1","GPIO_GREEN2","GPIO_YELLOW1","GPIO_YELLOW2","GPIO_RED1","GPIO_RED2"} ;

static char *speaker_desc = "GPIO_SPEAKER";

//static char *button_desc[] = {"GPIO_BUTTON1", "GPIO_BUTTON2"};


/****************************************************************************/
/* LEDs write/read using gpio kernel API                                    */
/****************************************************************************/


static void byte2leds(char ch)
{
    int i;
    int val = (int) ch;
	switch (ch >> 6){
		case 0:
			for(i=0; i<6; i++) gpio_set_value(LED_GPIOS[i], (val >> i) & 1);
			break;
		case 1:
			for(i=0; i<6; i++) if((val >> i) & 1) gpio_set_value(LED_GPIOS[i], 1);
			break;
		case 2:
			for(i=0; i<6; i++) if((val >> i) & 1) gpio_set_value(LED_GPIOS[i], 0);
			break;
		default:
			break;
	}
}


static char leds2byte(void)
{
    int val;
    char ch;
    int i;
    ch=0;

    for(i=0; i<6; i++)
    {
        val=gpio_get_value(LED_GPIOS[i]);
        ch= ch | (val << i);
    }
    return ch;
}

/****************************************************************************/
/* SPEAKER write using gpio kernel API                                    */
/****************************************************************************/

static void toSpeaker(char ch)
{
    int val = ch-48;
    if(val != 0) val = 1;
	gpio_set_value(SPEAKER_GPIO, val);
}


#define TAM 100
int flag;
char * buff;
char * lee;
char * esc; 

int estaLleno(void){
	int r = 0;
	if (((esc == buff + TAM)&&(lee == buff))||(esc+1 == lee)) r = 1;
	return r;
} 

int estaVacio(void){
	int r = 0;
	if (lee == esc) r = 1;
	return r;
}
/****************************************************************************/
/* Interrupts variables block                                               */
/****************************************************************************/
// Constantes para guardar la dirección de la interrupcion para cada botón
static short int irq_BUTTON1;
static short int irq_BUTTON2;


// text below will be seen in 'cat /proc/interrupt' command
#define GPIO_BUTTON1_DESC           "Boton 1"

// below is optional, used in more complex code, in our case, this could be
#define GPIO_BUTTON1_DEVICE_DESC    "Placa lab. DAC"



// text below will be seen in 'cat /proc/interrupt' command
#define GPIO_BUTTON2_DESC           "Boton 2"

// below is optional, used in more complex code, in our case, this could be
#define GPIO_BUTTON2_DEVICE_DESC    "Placa lab. DAC"

DECLARE_WAIT_QUEUE_HEAD(cola);
DEFINE_SEMAPHORE(semaforo);
DEFINE_SEMAPHORE(semaforo2);


static void f_timerHandler(unsigned long data)
{
	 enable_irq(data);
}

DEFINE_TIMER(timerHandler1, f_timerHandler,0,0); //timer para filtrar pulsaciones
DEFINE_TIMER(timerHandler2, f_timerHandler,0,0); //timer para filtrar pulsaciones

//Definimos el codigo a ejecutar por la interrupcion del boton 1
static void f_bottonHalf1 (unsigned long n){
	mod_timer(&timerHandler1, jiffies + HZ/4);
    down(&semaforo);
    if(!estaLleno()){
		*esc = '1';
		if(esc == buff + TAM) esc = buff;
		else esc++;
		up(&semaforo);
	
	}
	else up(&semaforo); 
	wake_up_interruptible(&cola);
}


//Definimos el codigo a ejecutar por la interrupcion del boton 2
static void f_bottonHalf2 (unsigned long n){
	mod_timer(&timerHandler2, jiffies + HZ/4); 
	down(&semaforo);
    if(!estaLleno()){
		*esc = '2';
		if(esc == buff + TAM) esc = buff;
		else esc++;
		up(&semaforo);
	}
	else up(&semaforo);
	wake_up_interruptible(&cola);
}

//Declaramos las tasklets a ejecutar por cada interrupcion
DECLARE_TASKLET(bottomHalf1, f_bottonHalf1,0);
DECLARE_TASKLET(bottomHalf2, f_bottonHalf2,0);


/****************************************************************************/
/* IRQ handler - fired on interrupt                                         */
/****************************************************************************/
static irqreturn_t r_irq_handler1(int irq, void *dev_id, struct pt_regs *regs) {

    // we will increment value in leds with button push
    // due to switch bouncing this hadler will be fired few times for every putton push
    
    disable_irq_nosync(irq);// Esto vale para deshabil int. para los multi push
    tasklet_schedule(&bottomHalf1);
    // para activar, void enable_irq(int irq);
	
    return IRQ_HANDLED;
}

static irqreturn_t r_irq_handler2(int irq, void *dev_id, struct pt_regs *regs) {

    // we will increment value in leds with button push
    // due to switch bouncing this hadler will be fired few times for every putton push
    disable_irq_nosync(irq);// Esto vale para deshabil int. para los multi push
    // para activar, void enable_irq(int irq);
    
    tasklet_schedule(&bottomHalf2);
     //NUEVO
	
    return IRQ_HANDLED;
}

/****************************************************************************/
/* LEDs device file operations                                              */
/****************************************************************************/

static ssize_t leds_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *ppos)
{

    char ch;

    if (copy_from_user( &ch, buf, 1 )) {
        return -EFAULT;
    }

    printk( KERN_INFO " (write) valor recibido: %d\n",(int)ch);

    byte2leds(ch);

    return 1;
}

static ssize_t leds_read(struct file *file, char __user *buf,
                         size_t count, loff_t *ppos)
{
    char ch;

    if(*ppos==0) *ppos+=1;
    else return 0;

    ch=leds2byte();

    printk( KERN_INFO " (read) valor entregado: %d\n",(int)ch);


    if(copy_to_user(buf,&ch,1)) return -EFAULT;

    return 1;
}

static const struct file_operations leds_fops = {
    .owner	= THIS_MODULE,
    .write	= leds_write,
    .read	= leds_read,
};

/****************************************************************************/
/* Speaker device file operations                                              */
/****************************************************************************/

static ssize_t speaker_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *ppos)
{

    char ch;

    if (copy_from_user( &ch, buf, 1 )) {
        return -EFAULT;
    }

    printk( KERN_INFO " (write) valor recibido: %d\n",(int)ch);

    toSpeaker(ch);

    return 1;
}



static const struct file_operations speaker_fop = {
    .owner	= THIS_MODULE,
    .write	= speaker_write,
};


/****************************************************************************/
/* Buttons device file operations                                              */
/****************************************************************************/

static int buttons_open(struct inode *inode, struct file *file)
{
	if (down_interruptible(&semaforo2)) return -ERESTARTSYS; // capturamos el semáforo
	if (flag) return -EFAULT;
	else flag = 1; 
	up(&semaforo2);   // liberamos el semáforo
	return 0;
}

static int buttons_release(struct inode *inode, struct file *file)
{
	if (down_interruptible(&semaforo2)) return -ERESTARTSYS; // capturamos el semáforo
	if (!flag) return -EFAULT;
	else flag = 0; 
	up(&semaforo2);   // liberamos el semáforo
	return 1;
}

static ssize_t buttons_read(struct file *file, char __user *buf,
                         size_t count, loff_t *ppos)
{
    char ch;
    if(down_interruptible(&semaforo)) return -ERESTARTSYS; //NUEVO: sección crítica necesaria
    while(estaVacio()){ //NUEVO: mejor un while y asegurasrse de que se cumple la condición
		up(&semaforo); // antes de bloquearnos, soltamos el semáforo
		if (wait_event_interruptible(cola,!estaVacio())) return -ERESTARTSYS;  //aquí faltaba el return del error si nos interrumpen en el bloqueo
		if (down_interruptible(&semaforo)) return -ERESTARTSYS; // al despertar esperamos por el semáforoait_event_interruptible(cola,!estaVacio());  
	}
    ch=*lee;
    if(lee == buff + TAM) lee = buff;
    else lee++;
    printk( KERN_INFO " (read) valor entregado: %d\n",(int)ch);
    if(copy_to_user(buf,&ch,1)) return -EFAULT;
    return 1;
}

static const struct file_operations buttons_fops = {
    .owner	= THIS_MODULE,
    .open	= buttons_open,
    .release = buttons_release,
    .read	= buttons_read,
};


/****************************************************************************/
/* LEDs device struct                                                       */

static struct miscdevice leds_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "leds",
    .fops	= &leds_fops,
};

/****************************************************************************/
/* Speaker device struct                                                       */

static struct miscdevice speaker_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "speaker",
    .fops	= &speaker_fop,
};

/****************************************************************************/
/* Buttons device struct                                                       */

static struct miscdevice buttons_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "buttons",
    .fops	= &buttons_fops,
};


/*****************************************************************************/
/* This functions requests GPIOs and configures interrupts */
/*****************************************************************************/

/*******************************
 *  request and init gpios for leds
 *******************************/

static int r_GPIO_config(void)
{
    int i;
    int res=0;
    for(i=0; i<6; i++)
    {
        if ((res=gpio_request_one(LED_GPIOS[i], GPIOF_INIT_LOW, led_desc[i])))
        {
            printk(KERN_ERR "GPIO request faiure: led GPIO %d %s\n",LED_GPIOS[i], led_desc[i]);
            return res;
        }
        gpio_direction_output(LED_GPIOS[i],0);
	}
	return res;
}

/*******************************
 *  request and init gpios for speaker
 *******************************/

static int r_GPIO_config2(void)
{
    int res=0; 
	if ((res=gpio_request_one(SPEAKER_GPIO, GPIOF_INIT_LOW, speaker_desc)))
    {
        printk(KERN_ERR "GPIO request faiure: speaker GPIO %d %s\n",SPEAKER_GPIO, speaker_desc);
        return res;
    }
	gpio_direction_output(SPEAKER_GPIO,0);
	return res;
}

/*******************************
 *  set interrup for button 1
 *******************************/

static int r_int_config(void)
{
	int res=0;
    if ((res=gpio_request(GPIO_BUTTON1, GPIO_BUTTON1_DESC))) {
        printk(KERN_ERR "GPIO request faiure: %s\n", GPIO_BUTTON1_DESC);
        return res;
    }

    if ( (irq_BUTTON1 = gpio_to_irq(GPIO_BUTTON1)) < 0 ) {
        printk(KERN_ERR "GPIO to IRQ mapping faiure %s\n", GPIO_BUTTON1_DESC);
        return irq_BUTTON1;
    }
	timerHandler1.data = irq_BUTTON1;
    printk(KERN_NOTICE "  Mapped int %d for button1 in gpio %d\n", irq_BUTTON1, GPIO_BUTTON1);

    if ((res=request_irq(irq_BUTTON1,
                    (irq_handler_t ) r_irq_handler1,
                    IRQF_TRIGGER_FALLING,
                    GPIO_BUTTON1_DESC,
                    GPIO_BUTTON1_DEVICE_DESC))) {
        printk(KERN_ERR "Irq Request failure\n");
        return res;
    }


    return res;
}

/*******************************
 *  set interrup for button 2
 *******************************/

static int r_int_config2(void)
{
	int res=0;
    if ((res=gpio_request(GPIO_BUTTON2, GPIO_BUTTON2_DESC))) {
        printk(KERN_ERR "GPIO request faiure: %s\n", GPIO_BUTTON2_DESC);
        return res;
    }

    if ( (irq_BUTTON2 = gpio_to_irq(GPIO_BUTTON2)) < 0 ) {
        printk(KERN_ERR "GPIO to IRQ mapping faiure %s\n", GPIO_BUTTON2_DESC);
        return irq_BUTTON2;
    }
	timerHandler2.data = irq_BUTTON2;
    printk(KERN_NOTICE "  Mapped int %d for button2 in gpio %d\n", irq_BUTTON2, GPIO_BUTTON2);

    if ((res=request_irq(irq_BUTTON2,
                    (irq_handler_t ) r_irq_handler2,
                    IRQF_TRIGGER_FALLING,
                    GPIO_BUTTON2_DESC,
                    GPIO_BUTTON2_DEVICE_DESC))) {
        printk(KERN_ERR "Irq Request failure\n");
        return res;
    }


    return res;
}

/*****************************************************************************/
/* This functions registers devices, requests GPIOs and configures interrupts */
/*****************************************************************************/

/*******************************
 *  register device for leds
 *******************************/

static int r_dev_config(void)
{
    int ret=0;
    ret = misc_register(&leds_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
    }
	else
		printk(KERN_NOTICE "misc_register OK... leds_miscdev.minor=%d\n", leds_miscdev.minor);
	return ret;
}

/*******************************
 *  request and init gpios for leds
 *******************************/
 
 /*******************************
 *  register device for speaker
 *******************************/

static int r_dev_config2(void)
{
    int ret=0;
    ret = misc_register(&speaker_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
    }
	else
		printk(KERN_NOTICE "misc_register OK... speaker_miscdev.minor=%d\n", speaker_miscdev.minor);
	return ret;
}

/*******************************
 *  request and init gpios for speaker
 *******************************/
 
  /*******************************
 *  register device for speaker
 *******************************/

static int r_dev_config3(void)
{
    int ret=0;
    ret = misc_register(&buttons_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
    }
	else
		printk(KERN_NOTICE "misc_register OK... buttons_miscdev.minor=%d\n", buttons_miscdev.minor);
	return ret;
}

/*******************************
 *  request and init gpios for speaker
 *******************************/
 
 
/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/


static void r_cleanup(void) {
    int i;
    printk(KERN_NOTICE "%s module cleaning up...\n", KBUILD_MODNAME);
    for(i=0; i<6; i++) gpio_free(LED_GPIOS[i]);	// libera GPIOS
    gpio_free(SPEAKER_GPIO); // Libera speaker
    if (leds_miscdev.this_device) misc_deregister(&leds_miscdev);
    printk(KERN_NOTICE "Done. Bye from %s module leds\n", KBUILD_MODNAME);
    if (speaker_miscdev.this_device) misc_deregister(&speaker_miscdev);
    printk(KERN_NOTICE "Done. Bye from %s module speaker\n", KBUILD_MODNAME);
    if (buttons_miscdev.this_device) misc_deregister(&buttons_miscdev);
    printk(KERN_NOTICE "Done. Bye from %s module buttons\n", KBUILD_MODNAME);
    if(irq_BUTTON1) free_irq(irq_BUTTON1, GPIO_BUTTON1_DEVICE_DESC);   //libera irq
    gpio_free(GPIO_BUTTON1);  // libera GPIO
    printk(KERN_NOTICE "Done. Bye from %s module irq 1\n", KBUILD_MODNAME);
    if(irq_BUTTON2) free_irq(irq_BUTTON2, GPIO_BUTTON2_DEVICE_DESC);   //libera irq
    gpio_free(GPIO_BUTTON2);  // libera GPIO
    printk(KERN_NOTICE "Done. Bye from %s module irq 2\n", KBUILD_MODNAME);
    vfree(buff);
    return;
}


static int r_init(void) {
    int res=0;
    
    printk(KERN_NOTICE "Hello, loading %s module!\n", KBUILD_MODNAME);
    
    printk(KERN_NOTICE "%s - GPIO config...\n", KBUILD_MODNAME);
    if((res = r_GPIO_config()))
    {
		r_cleanup();
		return res;
	}
    printk(KERN_NOTICE "%s - devices config...\n", KBUILD_MODNAME);

	if((res = r_GPIO_config2()))
    {
		r_cleanup();
		return res;
	}
    printk(KERN_NOTICE "%s - devices config...\n", KBUILD_MODNAME);

    if((res = r_dev_config()))
    {
		r_cleanup();
		return res;
	}
	
	if((res = r_dev_config2()))
    {
		r_cleanup();
		return res;
	}
	
	if((res = r_dev_config3()))
    {
		r_cleanup();
		return res;
	}
	
    printk(KERN_NOTICE "%s - int config...\n", KBUILD_MODNAME);
    if((res = r_int_config()))
    {
		r_cleanup();
		return res;
	}
	if((res = r_int_config2())){
		
		r_cleanup();
		return res;
	}
	buff = (char *) vmalloc(TAM * sizeof(char));
	lee = buff;
	esc = buff;
	flag = 0;    
    return res;
}

module_init(r_init);
module_exit(r_cleanup);

/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/
MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
