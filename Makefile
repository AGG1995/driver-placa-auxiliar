MODULE=interrupt
 
KERNEL_SRC=/lib/modules/`uname -r`/build
 
obj-m += ${MODULE}.o
 
compile:
	make -C ${KERNEL_SRC} M=${CURDIR} modules

install:
	sudo insmod ${MODULE}.ko 
	sudo chmod a+rw /dev/leds
	sudo chmod a+rw /dev/speaker
	sudo chmod a+rw /dev/buttons
	dmesg | tail 
	
uninstall:
	sudo rmmod ${MODULE} 
	dmesg | tail
 
 
 
